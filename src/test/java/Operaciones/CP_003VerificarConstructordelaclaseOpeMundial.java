/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

import clsClases.clsOpeMundial;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrestlo
 */
public class CP_003VerificarConstructordelaclaseOpeMundial {
    
    
    /*Atributos*/
    String strEquipo1Esperado; 
    String strEquipo1Obtenido;
    String strEquipo2Esperado;
    String strEquipo2Obtenido;
    String strErrorEsperado;
    String strErrorObtenido;
    String strHoraEsperado;
    String strHoraObtenido;
    Date   dtFechaEsperado;
    Date   dtFechaObtenido;
    
    
    public CP_003VerificarConstructordelaclaseOpeMundial() {
    }
    
    
    
    @Before
    public void setUp() {
        
        strEquipo1Esperado = "";
        strEquipo2Esperado = "";
        strErrorEsperado= "";
        strHoraEsperado = "";
        dtFechaEsperado = null;
        
        
        
    }
    
    @Test
    public void VerificarConstructordelaclaseOpeMundialSeEsperaAtributosNulos()
    {
        clsOpeMundial _objOpeMundial = new clsOpeMundial();
        
        strEquipo1Obtenido = _objOpeMundial.getEquipo1();
        strEquipo2Obtenido = _objOpeMundial.getEquipo2();
        strErrorObtenido = _objOpeMundial.getError();
        strHoraObtenido = _objOpeMundial.getHora();
        dtFechaObtenido = _objOpeMundial.getFecha();
        
        
        
    }
    
    @After
    public void tearDown() {
        
        if(strEquipo1Esperado != strEquipo1Obtenido)
        {
            throw new Error("el caso de prueba CP_003VerificarConstructordelaclaseOpeMundial fallo, se esperaba un valor " + strEquipo1Esperado + "y se obtuvo " + strEquipo1Obtenido);
        }
        
        if(strEquipo2Esperado != strEquipo2Obtenido)
        {
            throw new Error("el caso de prueba CP_003VerificarConstructordelaclaseOpeMundial fallo, se esperaba un valor " + strEquipo2Esperado + "y se obtuvo " + strEquipo2Obtenido);
        }
        
        
        if(strErrorEsperado != strErrorObtenido)
        {
            throw new Error("el caso de prueba CP_003VerificarConstructordelaclaseOpeMundial fallo, se esperaba un valor " + strErrorEsperado + "y se obtuvo " + strErrorObtenido);
        }
        
        
        if(strHoraEsperado != strHoraObtenido)
        {
            throw new Error("el caso de prueba CP_003VerificarConstructordelaclaseOpeMundial fallo, se esperaba un valor " + strHoraEsperado + "y se obtuvo " + strHoraObtenido);
        }
        
        
        if(dtFechaEsperado != dtFechaObtenido)
        {
            throw new Error("el caso de prueba CP_003VerificarConstructordelaclaseOpeMundial fallo, se esperaba un valor " + dtFechaEsperado + "y se obtuvo " + dtFechaObtenido);
        }
    }
    
}
