
package clsClases;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;


public class clsOpeMundial {

    public clsOpeMundial() {
        strEquipo1 = "";
        strEquipo2 = "";
        strError = "";
        strHora = "";
        dtFecha = null;
        
    }
    
    /*Atributos*/
    private String strEquipo1; 
    private String strEquipo2;
    private String strError; 
    private String strHora;
    private Date   dtFecha;

    
    /*Parametros*/
    
    public String getEquipo1() {
        return strEquipo1;
    }

    public String getEquipo2() {
        return strEquipo2;
    }

    public String getHora() {
        return strHora;
    }

    public Date getFecha() {
        return dtFecha;
    }

    /**
     * @param strEquipo1 the strEquipo1 to set
     */
    public void setEquipo1(String strEquipo1) {
        this.strEquipo1 = strEquipo1;
    }

    /**
     * @param strEquipo2 the strEquipo2 to set
     */
    public void setEquipo2(String strEquipo2) {
        this.strEquipo2 = strEquipo2;
    }
    /**
     * @return the strError
     */
    public String getError() {
        return strError;
    }
    
    public void setHora(String strHora) {
        this.strHora = strHora;
    }
    
    public void setFecha(Date dtFecha) {
        this.dtFecha = dtFecha;
    }
    
    
 private  boolean ValidarSeleccionar1(){
     if (strEquipo1.equals("Seleccione") || strEquipo2.equals("Seleccione")) {
         strError = "Debe seleccionar un equipo del grupo";
        return false; 
     }
     return true;
  }

 private boolean ValidarEquiposDiferentes(){
     if (strEquipo2.equals(strEquipo1)) {
         strError = "Debe seleccionar un equipo diferente";
         return false;
     }
     return true;
 }
 
 private boolean ValidarFecha() throws Exception
 {
     String Fecha1 = "2018-06-14";
     String Fecha2 = "2018-07-14";
     try {
         
     SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
     SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
     //sdf.format(dtFecha);
     Date dtFecha1 = sdf1.parse(Fecha1);
     Date dtFecha2 = sdf2.parse(Fecha2);
     
     if(dtFecha.before(dtFecha1) || dtFecha.after(dtFecha2) )
     {
         strError = "La fecha ingresada no corresponde al tiempo de duracion del mundial";
         return false;
         
     }
     
     return true;    
     
     } catch (Exception e) {
         throw e;
     }
     
 }
 
  private boolean ValidarHora(){
    boolean b;
    char[] a = strHora.toString().toCharArray();
    String[] c = strHora.split(":");
    
        if ((a[0] == ' ') || (a[1] == ' ') || (a[2] == ' ') || (a[3] == ' ') || (a[4] == ' ') || 
            (getInteger(c[0]) > 24) ||(getInteger(c[1]) > 59) ) {
            strError = "La hora ingresada no es válida";
            b = false;    
        }else{
                b = true;
        }
        return b;
  }
  
  public int getInteger(String strvalor){
  
     int integer = Integer.parseInt(strvalor);
     return integer;
  
  } 
  
  public void Guardar(){
      
      clsRNmundial objmund = new   clsRNmundial();
      try {
      
      
      
      if (ValidarSeleccionar1()) {
        
          if (ValidarEquiposDiferentes()) {
              
              if (ValidarHora()) {
                  
                  if(ValidarFecha()) {
                  
                  
                  objmund.setEquipo1(strEquipo1);
                  objmund.setEquipo2(strEquipo2);
                  objmund.setFecha(dtFecha);
                  objmund.setHora(strHora);
                  
                  
                  
                      objmund.EnviarVs();
                      strError = "Se ha guardado la información de los equipos correctamente";
                      
                  
                  }
                  
              }
              
          }
          
      }
  
  } catch (Exception e) {
                    strError = e.getMessage();
                      
   }
                  
}
}


 

