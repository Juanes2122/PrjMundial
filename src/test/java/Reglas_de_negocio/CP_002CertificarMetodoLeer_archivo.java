/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reglas_de_negocio;

import clsClases.clsRNmundial;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrestlo
 */
public class CP_002CertificarMetodoLeer_archivo {
    
    /*Atributos*/
    String[] strEquiposEsperado = new String[4];
    String[] strEquiposObtenido = new String[4];
    clsRNmundial _objRNmundialCP2;
    
    public CP_002CertificarMetodoLeer_archivo() {
    }
    
    @Before
    public void setUp() {
        
        strEquiposEsperado[0]= "Rusia";
        strEquiposEsperado[1]= "Uruguay";
        strEquiposEsperado[2]= "Arabia";
        strEquiposEsperado[3]= "Egipto";
        _objRNmundialCP2 = new clsRNmundial();
    }
    
    @Test
    public void CertificarMetodoLeer_archvo()
    {
        
        _objRNmundialCP2.setRutaArchivo("D:\\Excel\\Mundial.xlsx");
        _objRNmundialCP2.setNumHoja(0);
        
        _objRNmundialCP2.leer_archivo();
        
        for(int i = 0; i < 4; i++)
        {
            strEquiposObtenido[i] = _objRNmundialCP2.getEquipos(i);
        }
        
    }
    
    @After
    public void tearDown() {
        
        for(int i = 0; i < 4; i++)
        {
            if(!strEquiposEsperado[i].trim().equals(strEquiposObtenido[i].trim()))
            {
                throw new Error("el caso de prueba CP_002CertificarMetodoLeer_archivo fallo, se esperaba " + strEquiposEsperado[i] + " y se obtuvo " + strEquiposObtenido[i]);
            }
        }
        
    }
    
}
