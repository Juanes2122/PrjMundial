/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clsClases;


/*import para leer excel*/
 
import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class clsRNmundial {


 /*ATRIBUTOS*/
    private String rutaArchivo;
    private String [] equipos; //get1
    private int numHoja;
    private String strEquipo1;
    private String strEquipo2;
    private Date dtFecha;
    private String strError; //get2
    private String strHora;

    
   
    public clsRNmundial(){
        
        rutaArchivo = null;
        equipos = new String[4];
        numHoja = -1;
        strEquipo1=null;
        strEquipo2=null;
        dtFecha=null;
        strError=null;
        strHora = null;
    }
    
    // propiedades
        /**
     * @return the equipos
     */
    

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }
    
    public void setNumHoja(int numHoja) {
        this.numHoja = numHoja;
    }
    
    public void setEquipo1(String strEquipo1) {
        this.strEquipo1 = strEquipo1;
    }

    public void setEquipo2(String strEquipo2) {
        this.strEquipo2 = strEquipo2;
    }

    public void setFecha(Date dtFecha) {
        this.dtFecha = dtFecha;
    }
    
    public void setHora(String strHora) {
        this.strHora = strHora;
    }
    
    public String getEquipos(int index) {
        return equipos[index];
    }
    
    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public int getNumHoja() {
        return numHoja;
    }

    public String getEquipo1() {
        return strEquipo1;
    }

    public String getEquipo2() {
        return strEquipo2;
    }

    public Date getFecha() {
        return dtFecha;
    }

    public String getHora() {
        return strHora;
    }
    
     public String getError() {
        return strError;
    }
    
    
    public void leer_archivo(){
    
        try (FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
            // leer archivo excel
            XSSFWorkbook worbook = new XSSFWorkbook(file);
            //obtener la hoja que se va leer
            XSSFSheet sheet = worbook.getSheetAt(numHoja);
            //obtener todas las filas de la hoja excel
            Iterator<Row> rowIterator = sheet.iterator();

            Row row;
            // se recorre cada fila hasta el final
                    int i = 0;
                    while (rowIterator.hasNext()) {
                        
                        row = rowIterator.next();
                        //se obtiene las celdas por fila
                        Iterator<Cell> cellIterator = row.cellIterator();
                        Cell cell;
                        //se recorre cada celda
                        while (cellIterator.hasNext()) {

                                // se obtiene la celda en específico y se la imprime
                                cell = cellIterator.next();
                                equipos[i] = cell.getStringCellValue();
                        }
                        i = i + 1;
                    }
            } catch (Exception e) {
                    e.getMessage();
            }

    }
    
    public void EnviarVs() throws Exception
    {
        try {
            
            File _objDirectorio = new File("D:\\TXT\\equiposVS.txt");            
                      
            if (!_objDirectorio.exists()) {
               _objDirectorio.createNewFile();
            }
            
            FileWriter _objFW = new FileWriter(_objDirectorio);
            
            BufferedWriter _objBW = new BufferedWriter(_objFW); // especificar en que archivo se va a escribir
            
            PrintWriter _objPW = new PrintWriter(_objBW);
            
            String strPartidoyFecha = "\nEquipos: " + strEquipo1 + " VS " + strEquipo2 +" Fecha: "+formatfecha(dtFecha) +" Hora: "+ strHora;
                                   
            _objPW.write(strPartidoyFecha);
            
            _objPW.close();
            
            _objBW.close();
            
            
                                   
            
        } catch (IOException e) {
            throw e;
        }
    }
    
    private String formatfecha(Date dtfecha){
        
        Format _objformatter = new SimpleDateFormat("yyyy-MM-dd");
        return _objformatter.format(dtfecha);
    
    }

    
   

   

   


}
