package Reglas_de_negocio;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import clsClases.clsRNmundial;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jrestlo
 */
public class CP_001CerificarconstructordelaClaseRNmundial {
    
    /*Atributos*/
    String strrutaArchivoEsperada;
    String strrutaArchivoObtenido;
    String [] equiposEsperado = new String[4];
    String [] equiposObtenido = new String[4];// este no se como vaa 
    int intnumHojaEsperado;
    int intnumHojaObtenido;
    String strEquipo1Esperado;
    String strEquipo1Obtenido;
    String strEquipo2Esperado;
    String strEquipo2Obtenido;
    Date dtFechaEsperado;
    Date dtFechaObtenido;
    String strErrorEsperado;
    String strErrorObtenido;
    String strHoraEsperada;
    String strHoraObtenida;
    
    public CP_001CerificarconstructordelaClaseRNmundial() {
    }
    
    @Before
    public void setUp() {
        
        strrutaArchivoEsperada = null;
        
        for(int i = 0; i < 4; i++)
        {
        equiposEsperado[i] = null;
        }
        
        equiposObtenido = null;
        intnumHojaEsperado = -1;
        strEquipo1Esperado = null;
        strEquipo2Esperado = null;
        dtFechaEsperado = null;
        strErrorEsperado = null;
        strHoraEsperada = null;
        
        
        
    }
    
    @Test
    public void VerificarConstructorRNmundialesperovaloresnulos()
    {
        
        
        clsRNmundial _objRNmundial = new clsRNmundial();
        
               
        for(int i = 0; i < 4; i++)
        {
        equiposObtenido[i] = _objRNmundial.getEquipos(i);
        }
        strrutaArchivoObtenido = _objRNmundial.getRutaArchivo();
        intnumHojaObtenido = _objRNmundial.getNumHoja();
        strEquipo1Obtenido = _objRNmundial.getEquipo1();
        strEquipo2Obtenido = _objRNmundial.getEquipo2();
        dtFechaObtenido = _objRNmundial.getFecha();
        strErrorObtenido = _objRNmundial.getError();
        strHoraObtenida = _objRNmundial.getHora();     
        
    }
    
    @After
    public void tearDown() {
      
        if(strrutaArchivoEsperada != strrutaArchivoObtenido)
        {
            throw new Error("el caso de prueba CP_001CerificarconstructordelaClaseRNmundial fallo, se esperaba " + strrutaArchivoEsperada + " y se obtuvo " + strrutaArchivoObtenido);
        }
        
        for(int i = 0; i < 4; i++)
        {
        if(equiposEsperado[i] != equiposObtenido[i])
        {
            throw new Error("el caso de prueba CP_001CerificarconstructordelaClaseRNmundial fallo, se esperaba " + equiposEsperado[i] + " y se obtuvo " + equiposObtenido[i]);
        }
        }
        
        if(intnumHojaEsperado != intnumHojaObtenido)
        {
            throw new Error("el caso de prueba CP_001CerificarconstructordelaClaseRNmundial fallo, se esperaba " + intnumHojaEsperado + " y se obtuvo " + intnumHojaObtenido);
        }
        
        if(strEquipo1Esperado != strEquipo1Obtenido)
        {
            throw new Error("el caso de prueba CP_001CerificarconstructordelaClaseRNmundial fallo, se esperaba " + strEquipo1Esperado + " y se obtuvo " + strEquipo1Obtenido);
        }
        
        
        if(strEquipo2Esperado != strEquipo2Obtenido)
        {
            throw new Error("el caso de prueba CP_001CerificarconstructordelaClaseRNmundial fallo, se esperaba " + strEquipo2Esperado + " y se obtuvo " + strEquipo2Obtenido);
        }
        
        
        if(dtFechaEsperado != dtFechaObtenido)
        {
            throw new Error("el caso de prueba CP_001CerificarconstructordelaClaseRNmundial fallo, se esperaba " + dtFechaEsperado + " y se obtuvo " + dtFechaObtenido);
        }
        
        
        if(strErrorEsperado != strErrorObtenido)
        {
            throw new Error("el caso de prueba CP_001CerificarconstructordelaClaseRNmundial fallo, se esperaba " + strErrorEsperado + " y se obtuvo " + strErrorObtenido);
        }
        
        if(strHoraEsperada != strHoraObtenida)
        {
            throw new Error("el caso de prueba CP_001CerificarconstructordelaClaseRNmundial fallo, se esperaba " + strHoraEsperada + " y se obtuvo " + strHoraObtenida);
            
        }
        
    }
    
}
